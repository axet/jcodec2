# jcodec2

Codec 2 java library is a low-bitrate speech audio codec (speech coding) that is patent free and open-source.

# Features over standart repo:

  * maven support
  * rearrange classes

Original port (line to line from C):

  * http://dmilvdv.narod.ru/Apps/codec2.html

Original C project:

  * http://www.rowetel.com

Wikipedia:

  * https://en.wikipedia.org/wiki/Codec_2